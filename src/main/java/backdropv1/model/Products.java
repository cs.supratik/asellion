package backdropv1.model;

import javax.persistence.*;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

@Entity
@Table(name = "products")
@ApiModel(description = "New Product Registration Model")
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(notes = "Product id of the Product, Auto generated")
	private Long Id;

	@Column(name = "name", length = 500)
	@ApiModelProperty(notes = "The name of the Product, max char allowed : 500")
	private String name;

	@Column(name = "currentPrice")
	@ApiModelProperty(notes = "The currentPrice of the Product, Type: Double")
	private Double price;

	@Column(name = "currency", length = 5)
	@ApiModelProperty(notes = "currency, max char allowed : 5")
	private String currency;

	@Column(name = "category", length = 8 )
	@ApiModelProperty(notes = "category, should be at max 8 characters")
	private String category;

	@Column(name = "lastUpdate")
	private LocalDateTime lastUpdate;

	
	public Products() {
		super();
	}

	public Products(String name, Double price, String currency, String category) {

		this.name = name;
		this.price = price;
		this.currency = currency;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
}
