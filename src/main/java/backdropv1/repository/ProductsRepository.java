package backdropv1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import backdropv1.model.Products;

import java.util.List;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Long>{
    public List<Products> findByNameContainingIgnoreCase(String name);
    public List<Products> findByCurrencyContainingIgnoreCase(String currency);
    public List<Products> findByCategoryContainingIgnoreCase(String category);

}
