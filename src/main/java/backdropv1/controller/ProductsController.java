package backdropv1.controller;

import java.time.LocalDateTime;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import backdropv1.model.Products;
import backdropv1.repository.ProductsRepository;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class ProductsController {
	@Autowired
	private ProductsRepository nuRepository;

	Logger elog = LoggerFactory.getLogger(ProductsController.class);
	

	@GetMapping("products")
	@ApiOperation(value = "Find all Product details",
			notes = "Provide an existing ProductName and the api returns all Product details as List<Products>",
			response = List.class)
	public ResponseEntity<List<Products>> getAllProducts(@RequestParam(required = false) String name) {

		elog.trace("queryall Rest api is invoked with query param: "+name);

		List<Products> listOfProd = new ArrayList<>();
		try{
			if (name == null)
				listOfProd = this.nuRepository.findAll();
			else
				listOfProd = this.nuRepository.findByNameContainingIgnoreCase(name);
			if(listOfProd.isEmpty()) {
				return new ResponseEntity<>(listOfProd, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(listOfProd,HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@GetMapping("products/{productid}")
	@ApiOperation(value = "Find User details for the given input Products id",
	notes = "Provide an existing Product id and the api returns Products details as Products",
	response = Products.class)
	public ResponseEntity<Products> getProductsById(@PathVariable(value = "productid") Long pid)   {
		Optional<Products> prod1 = this.nuRepository.findById(pid);
		
		if(prod1.isPresent())
			return new ResponseEntity<>(prod1.get(),HttpStatus.OK);
		else
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	  }

	@PostMapping("products")
	@ApiOperation(value = "Insert new Product API",
	notes = " input Name, currentPrice, currency, category and the api insert record in database",
	response = Products.class)
	public ResponseEntity<Products> createNewProduct(@Validated @RequestBody Products prd) {

		try {
			prd.setLastUpdate(LocalDateTime.now());
			Products insertedProduct = this.nuRepository
					.save(prd);
			return new ResponseEntity<>(insertedProduct, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("products/{productid}")
	@ApiOperation(value = "Update existing Product API",
			notes = " input Name, currentPrice, currency, category and the api updates record in database for the given Id",
			response = Products.class)
	public ResponseEntity<Products> updateProduct(@PathVariable(required = true,value = "productid") Long pid, @Validated @RequestBody Products prd) {
		try {
			Optional<Products> productData = this.nuRepository.findById(pid);

			if (productData.isPresent()) {
				Products prod1 = productData.get();
				prod1.setName(prd.getName());
				prod1.setPrice(prd.getPrice());
				prod1.setCurrency(prd.getCurrency());
				prod1.setCategory(prd.getCategory());
				prod1.setLastUpdate(LocalDateTime.now());
				return new ResponseEntity<>(this.nuRepository.save(prod1), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("products/{productid}")
	@ApiOperation(value = "Delete existing Product for the given product id",
	notes = "Provide an existing product id and the api returns deleted status as Map<String,String>",
	response = Map.class)
	public ResponseEntity<Map<String, Boolean>> deleteProduct(@PathVariable (value = "productid") Long pid)  {
		Optional<Products> productData = this.nuRepository.findById(pid);

		try {
			if (productData.isPresent()) {
				this.nuRepository.deleteById(pid);
				Map<String, Boolean> responseData = new HashMap();
				responseData.put("deleted", Boolean.TRUE);
				return  new ResponseEntity<>(responseData,HttpStatus.OK);
			} else
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
